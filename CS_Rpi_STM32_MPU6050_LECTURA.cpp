/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */
#include "mbed.h"
#define MPU6050_address 0xD0
//ESCALAS DEL GIROSCOPIO
#define GFS_SEL_250 0x00
#define GFS_SEL_500 0x08
#define GFS_SEL_1000 0x10
#define GFS_SEL_2000 0x18
//ESCALAS DEL ACELERÓMETRO
#define AFS_SEL_2G 0X00
#define AFS_SEL_4G 0X08
#define AFS_SEL_8G 0X10
#define AFS_SEL_16G 0X18
//ESCALAS DE CONVERSIÓN DEL ACELERÓMETRO,GIROSCOPIO Y TEMPERATURA
#define SENSITIVITY_ACCEL 2.0/32768.0
#define SENSITIVITY_GYRO 250.0/32768.0
#define SENSITIVITY_TEMP 340.0
#define TEMP_OFFET 20

//DECLARACIÓN DE LAS VARIABLES
int16_t raw_Accelx,raw_Accely,raw_Accelz;
int16_t raw_Gyrox,raw_Gyroy,raw_Gyroz;
int16_t raw_Temp;
//Valores "RAW" de tipo flotante para el acelerómetro
float Raw_Accelx,Raw_Accely,Raw_Accelz;
//Variables "Offsets" tipo flotante Acelerometro
float Offset_Accelx=-1166.00;
float Offset_Accely=-98.00;
float Offset_Accelz=15822.00;
//Valores "RAW" de tipo flotante para el Giroscopio
float Raw_Gyrox,Raw_Gyroy,Raw_Gyroz;
//Variables "Offsets" tipo flotante Giroscopio
float Offset_Gyrox=244.50;
float Offset_Gyroy=-43.50;
float Offset_Gyroz=-143.50;
//Salidas no calibradas
float accelx,accely,accelz;
float gyrox,gyroy,gyroz;
float Temp;
//Salidas calibradas
float Accelx,Accely,Accelz;
float Gyrox,Gyroy,Gyroz;
//Bytes
int j ;
char reg[2];
char data[1];
char GyrAcel[14];
float buffer[500][8];
float timer=0;
Timer t;
///Inicialización de la comunicación por I2C
Serial pc(SERIAL_TX,SERIAL_RX);
I2C i2c(PB_9,PB_8);
//Inicio del programa
int main()
{
    reg[0]=0x6B;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    //CONFIGURACIÓN DEL ACELERÓMETRO A UNA ESCALA DE 250 deg/s
    reg[0]=0x1B;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    //CONFIGURACIÓN DEL ACELERÓMETRO A UNA ESCALA CON UN RANGO DE 2 g
    reg[0]=0x1C;
    reg[1]=0x00;
    i2c.write(MPU6050_address,reg,2);
    wait(0.01);
    while (1) {
        if(pc.readable()) {
            if(pc.getc()=='I') {
                for(j=0; j<100; j++) {
                    reg[0]=0x3B;
                    i2c.write(MPU6050_address,reg,1);
                    i2c.read(MPU6050_address,GyrAcel,14);
                    t.reset();
                    t.start();
                    raw_Accelx= GyrAcel[0]<<8 |GyrAcel[1];
                    raw_Accely= GyrAcel[2]<<8 |GyrAcel[3];
                    raw_Accelz= GyrAcel[4]<<8 |GyrAcel[5];
                    raw_Temp=   GyrAcel[6]<<8 |GyrAcel[7];
                    raw_Gyrox=  GyrAcel[8]<<8 |GyrAcel[9];
                    raw_Gyroy=  GyrAcel[10]<<8 |GyrAcel[11];
                    raw_Gyroz=  GyrAcel[12]<<8 |GyrAcel[13];
                   
                    wait_us(8380);
                    t.stop();
                    timer=t.read();
                    pc.printf("%d %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n\r",j,timer,(float)raw_Accelx,(float)raw_Accely,(float)raw_Accelz,(float)raw_Gyrox,(float)raw_Gyroy,(float)raw_Gyroz);
                }
            }
        }
    }
}
