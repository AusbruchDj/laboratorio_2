%Comunicación serial entre STM327G7ZI Y MATLAB para la adquisición de datos
%de la MPU6050 
close all;
clear all;
clc;
Objold=instrfind;
if not (isempty (Objold))
    fclose(Objold);
    delete(Objold);
end 
if ~exist('s','var')
    s=serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if ~isvalid(s)
    s=serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if strcmp(get(s,'status'),'closed')
    fopen(s);
end
SENSITIVY_ACCEL=2.0/32768.0;
SENSITIVY_GYRO=250.0/32768.0;

offset_accelx=506.00;
offset_accely=-208.00;
offset_accelz=16106.00;
offset_gyrox=225.50;
offset_gyroy=-40.00;
offset_gyroz=-109.00;

disp('Inicio de la calibracion')
pause();
disp('Comenzar')
fprintf(s,'I');
j = 1;
while(1)
    str{j}=fscanf(s);
    if(str{j}(2)=='F')
        disp('finalización')   
        break;
    end
    j= j + 1;    
end

fclose(s);
n=length(str)-1;
C=1;
for i=1:n
    Temp=cellfun(@str2num,strsplit(str{i},','));
    if numel(Temp) == 8
        values(i,:)=Temp;
    end
end

Nsamples=length(values);
dt=0.01;
t=0:dt:Nsamples*dt-dt;
%--------------------------------------------------------------------------
 %                       Acelerometro sin calibrar 
%--------------------------------------------------------------------------
figure;
plot(t,values(:,3)*SENSITIVY_ACCEL,'b');
hold on 
plot(t,values(:,4)*SENSITIVY_ACCEL,'r');
plot(t,values(:,5)*SENSITIVY_ACCEL,'g');
title('Acelerometro sin calibración')
ylabel('Aceleración (g)');
xlabel('Tiempo (s)');
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
 %--------------------------------------------------------------------------
 %                       Acelerometro con calibrarción 
%--------------------------------------------------------------------------   
figure;
plot(t,(values(:,3)-offset_accelx)*SENSITIVY_ACCEL,'b');
hold on 
plot(t,(values(:,4)-offset_accely)*SENSITIVY_ACCEL,'r');
plot(t,(values(:,5)-(offset_accelz-(32768/2)))*SENSITIVY_ACCEL,'g');
title('Acelerometro calibrado')
ylabel('Aceleración (g)');
xlabel('Tiempo (s)');
legend('ax','ay','az','Location','northeast','Orientation','horizontal');
%--------------------------------------------------------------------------
 %                         Giroscopio sin calibrar 
%--------------------------------------------------------------------------
figure;
plot(t,values(:,6)*SENSITIVY_GYRO,'b');
hold on 
plot(t,values(:,7)*SENSITIVY_GYRO,'r');
plot(t,values(:,8)*SENSITIVY_GYRO,'g');
title('Giroscopio sin calibración')
ylabel('Velocidad angular (°/s)');
xlabel('Tiempo (s)');
legend('gx','gy','gz','Location','northeast','Orientation','horizontal')
%--------------------------------------------------------------------------
 %                         Giroscopio calibrado
%--------------------------------------------------------------------------
figure;
plot(t,(values(:,6)-offset_gyrox)*SENSITIVY_GYRO,'b');
hold on 
plot(t,(values(:,7)-offset_gyroy)*SENSITIVY_GYRO,'r');
plot(t,(values(:,8)-offset_gyroz)*SENSITIVY_GYRO,'g');
title('Giroscopio calibrado')
ylabel('Velocidad angular (°/s)');
xlabel('Tiempo (s)');
legend('gx','gy','gz','Location','northeast','Orientation','horizontal')
