import serial
import numpy
import matplotlib.pyplot as plt
import numpy.matlib as npm
from copy import copy,deepcopy

SENSITIVITY_ACCEL=2.0/32768.0
SENSITIVITY_GYRO=250.0/32768.0

Data=serial.Serial('/dev/ttyACM0',9600,timeout=5)
print (Data)
Datos=numpy.zeros((100,8))
Datos1=numpy.zeros((100,8))
Datos2=numpy.zeros((100,8))

valor=input("\n Quiere iniciar con la adquisición de datos S/N \n\n ")

if valor=='S' or valor=='s':
    print("\n Obteniendo los datos \n")
    Data.write (b'I')
    for i in range (100):
        rec=Data.readline()
        print(rec)
        rec=rec.decode('utf-8')
        print (rec)
        rec=rec.split()
        print(rec)
        Datos[i][:]=rec
    print("\n Fin de la adquisicion de datos \n")
    print(Datos,"\n")
    print(type(Datos))
    print (type(Datos[0,1]),type (Datos[0][1]))

    Offsets = [numpy.mean(Datos[:,2]),numpy.mean(Datos[:,3]),numpy.mean(Datos[:,4])-(32768/2),numpy.mean(Datos[:,5]),numpy.mean (Datos[:,6]), numpy.mean(Datos[:,7])]                                          
    print(Offsets)
    Datos1=deepcopy(Datos)
    Datos2=deepcopy(Datos)
    
    #Plotear los datos de el acelerometro y giroscopio si calibración
    for a in range(0,3):
        for b in range (0,100):
            Datos1[b] [a+2]=((Datos1[b,a+2]))*SENSITIVITY_ACCEL
            Datos1[b] [a+5]=((Datos1[b,a+5]))*SENSITIVITY_GYRO
        #Plotear datos del acelerometro no calibrado 
        v1=plt.figure(1)
        ax1=v1.subplots(2,2)
        v1.suptitle('Acelerometro no calibrado MPU6050')
        ax1[0,0].plot(Datos1[:,0],Datos1[:,2])
        ax1[0,0].set_title('ax')
        ax1[0,1].plot(Datos1[:,0],Datos1[:,3])
        ax1[0,1].set_title('ay')
        ax1[1,0].plot(Datos1[:,0],Datos1[:,4])
        ax1[1,0].set_title('az')
        ax1[1,1].plot(Datos1[:,0],Datos1[:,(2,3,4)])
        ax1[1,1].set_title('ax,ay y az')
        v1.show()
         #Plotear datos del giroscopio no calibrado 
        v2=plt.figure(2)
        ax2=v2.subplots(2,2)
        v2.suptitle('Giroscopío no calibrado MPU6050')
        ax2[0,0].plot(Datos1[:,0],Datos1[:,5])
        ax2[0,0].set_title('gx')
        ax2[0,1].plot(Datos1[:,0],Datos1[:,6])
        ax2[0,1].set_title('gy')
        ax2[1,0].plot(Datos1[:,0],Datos1[:,7])
        ax2[1,0].set_title('gz')
        ax2[1,1].plot(Datos1[:,0],Datos1[:,(5,6,7)])
        ax2[1,1].set_title('gx,gy y gz')
        v2.show()
    
        #Plotear los datos de el acelerometro y giroscopiuo si calibración                  
        for y in range(0,3):
            for z in range(0,100):
              Datos2[z] [y+2]=((Datos2[z,y+2])-Offsets[y])*SENSITIVITY_ACCEL
              Datos2[z] [y+5]=((Datos2[z,y+5])-Offsets[y+3])*SENSITIVITY_GYRO
        #Plotear datos del acelerometro calibrado 
        v3=plt.figure(3)
        ax3=v3.subplots(2,2)
        v3.suptitle('Acelerometro calibrado MPU6050')
        ax3[0,0].plot(Datos2[:,0],Datos2[:,2])
        ax3[0,0].set_title('ax')
        ax3[0,1].plot(Datos2[:,0],Datos2[:,3])
        ax3[0,1].set_title('ay')
        ax3[1,0].plot(Datos2[:,0],Datos2[:,4])
        ax3[1,0].set_title('az')
        ax3[1,1].plot(Datos2[:,0],Datos2[:,(2,3,4)])
        ax3[1,1].set_title('ax,ay y az')
        v3.show()
        #Plotear datos del giroscopio calibrado
        v4=plt.figure(4)
        ax4=v4.subplots(2,2)
        v4.suptitle('Giroscopío calibrado MPU6050')
        ax4[0,0].plot(Datos2[:,0],Datos2[:,5])
        ax4[0,0].set_title('gx')
        ax4[0,1].plot(Datos2[:,0],Datos2[:,6])
        ax4[0,1].set_title('gy')
        ax4[1,0].plot(Datos2[:,0],Datos2[:,7])
        ax4[1,0].set_title('gz')
        ax4[1,1].plot(Datos2[:,0],Datos2[:,(5,6,7)])
        ax4[1,1].set_title('gx,gy y gz')
        v4.show()        
       
